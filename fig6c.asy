import graph;
import unicode;
import interpolate; 
texpreamble("\usepackage{mathtext}\usepackage[russian]{babel}");
defaultpen(font("T2A","cmr","m","n"));
locale("it_IT");
usepackage("icomma");
defaultpen(0.4pt + fontsize(10pt));

real[] z_exp = { 0, 0.003, 0.006, 0.012, 0.021, 0.029, 0.047, 0.05, 0.062, 0.076, 0.088, 0.1, 0.103, 0.115, 0.126, 0.147, 0.15, 0.165, 0.176, 0.188, 0.2, 0.212, 0.217, 0.22, 0.223, 0.226, 0.229 };
real[] sigma_x_exp = { -481.75, -493.5, -502.9, -517, -533.45, -547.55, -564, -564, -559.3, -538.15, -517, -493.5, -486.45, -458.25, -430.05, -364.25, -352.5, -305.5, -253.8, -199.75, -141, -70.5, -35.25, -11.75, 0, 11.75, 11.75 }; 

real[] z_calc = { 0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2, 0.21, 0.22, 0.23, 0.24, 0.25, 0.26, 0.27, 0.28, 0.29, 0.3 };
real[] sigma_x_calc = { -684.9450891, -687.0268692, -687.7659546, -688.058685, -688.182291, -688.2292809, -688.2292809, -688.182291, -688.058685, -687.7659546, -687.0268692, -684.9450891, -678.4697043, -658.6543872, -612.7406442, -544.4627499, -469.0538685, -394.3341396, -322.9547157, -256.1834412, -194.8395492, -139.4367894, -90.2183517, -47.1906126, -10.1632581, 21.2069637, 47.3647401, 68.7996882, 86.0289912, 99.6020091, 110.0822283 };

picture pic1;

// графики
draw(pic1, graph(z_exp, sigma_x_exp, Hermite(monotonic)),linewidth(0.8pt));
draw(pic1, graph(z_calc, sigma_x_calc, Hermite(monotonic)),dashed+linewidth(0.8pt));

// пределы осей
xlimits(pic1, 0, 0.3);
ylimits(pic1, -800, 200);

// подписи к осям
xaxis(pic1,Label(shift(10,-2)*"$z, мм$",1),BottomTop);
yaxis(pic1,Label(shift(20,12)*"$\sigma_x$, МПа",1),LeftRight);
label(pic1, "$0$",(0, -800), S);
label(pic1, "$0.05$",(0.05, -800), S);
label(pic1, "$0.1$",(0.1, -800), S);
label(pic1, "$0.15$",(0.15, -800), S);
label(pic1, "$0.2$",(0.2, -800), S);
label(pic1, "$0.25$",(0.25, -800), S);
label(pic1, "$-800$",(0, -800), W);
label(pic1, "$-600$",(0, -600), W);
label(pic1, "$-400$",(0, -400), W);
label(pic1, "$-200$",(0, -200), W);
label(pic1, "$0$",(0, 0), W);

// штриховка
xequals(pic1, 0.05, Dotted);
xequals(pic1, 0.1, Dotted);
xequals(pic1, 0.15, Dotted);
xequals(pic1, 0.2, Dotted);
xequals(pic1, 0.25, Dotted);
yequals(pic1, -600, Dotted);
yequals(pic1, -400, Dotted);
yequals(pic1, -200, Dotted);
yequals(pic1, 0);

// размер изображения
size(pic1,6cm,7cm, point(pic1,SW), point(pic1,NE));
frame f1=pic1.fit();
add(f1);

