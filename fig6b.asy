import graph;
import unicode;
import interpolate; 
texpreamble("\usepackage{mathtext}\usepackage[russian]{babel}");
defaultpen(font("T2A","cmr","m","n"));
locale("it_IT");
usepackage("icomma");
defaultpen(0.4pt + fontsize(10pt));

real[] z_exp = { 0, 0.003, 0.006, 0.012, 0.021, 0.029, 0.047, 0.05, 0.062, 0.076, 0.088, 0.1, 0.103, 0.115, 0.126, 0.147, 0.15, 0.165, 0.176, 0.188, 0.2, 0.212, 0.217, 0.22, 0.223, 0.226, 0.229 };
real[] sigma_x_exp = { -481.75, -498.2, -507.6, -533.45, -564, -587.5, -632.15, -634.5, -634.5, -622.75, -611, -592.2, -587.5, -564, -540.5, -481.75, -470, -411.25, -352.5, -282, -216.2, -129.25, -98.7, -77.55, -51.7, -23.5, 10 }; 

real[] z_calc = { 0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2, 0.21, 0.22, 0.23, 0.24, 0.25, 0.26, 0.27, 0.28, 0.29, 0.3 };
real[] sigma_x_calc = { -685.0153287, -686.9924361, -687.7195533, -688.0152267, -688.1422662, -688.1912181, -688.1914143, -688.1432472, -688.0175811, -687.725145, -687.0066606, -685.0557459, -679.2396912, -661.9338702, -621.0564831, -557.2735308, -484.5422889, -411.4548459, -341.0386659, -274.6465479, -213.1401042, -157.091454, -106.8202071, -62.4206376, -23.7947436, 9.3112596,37.265247, 60.4960137,  79.460019, 94.6461933, 106.5722103 };

picture pic1;

// графики
draw(pic1, graph(z_exp, sigma_x_exp, Hermite(monotonic)),linewidth(0.8pt));
draw(pic1, graph(z_calc, sigma_x_calc, Hermite(monotonic)),dashed+linewidth(0.8pt));

// пределы осей
xlimits(pic1, 0, 0.3);
ylimits(pic1, -800, 200);

// подписи к осям
xaxis(pic1,Label(shift(10,-2)*"$z, мм$",1),BottomTop);
yaxis(pic1,Label(shift(20,12)*"$\sigma_x$, МПа",1),LeftRight);
label(pic1, "$0$",(0, -800), S);
label(pic1, "$0.05$",(0.05, -800), S);
label(pic1, "$0.1$",(0.1, -800), S);
label(pic1, "$0.15$",(0.15, -800), S);
label(pic1, "$0.2$",(0.2, -800), S);
label(pic1, "$0.25$",(0.25, -800), S);
label(pic1, "$-800$",(0, -800), W);
label(pic1, "$-600$",(0, -600), W);
label(pic1, "$-400$",(0, -400), W);
label(pic1, "$-200$",(0, -200), W);
label(pic1, "$0$",(0, 0), W);

// штриховка
xequals(pic1, 0.05, Dotted);
xequals(pic1, 0.1, Dotted);
xequals(pic1, 0.15, Dotted);
xequals(pic1, 0.2, Dotted);
xequals(pic1, 0.25, Dotted);
yequals(pic1, -600, Dotted);
yequals(pic1, -400, Dotted);
yequals(pic1, -200, Dotted);
yequals(pic1, 0);

// размер изображения
size(pic1,6cm,7cm, point(pic1,SW), point(pic1,NE));
frame f1=pic1.fit();
add(f1);

