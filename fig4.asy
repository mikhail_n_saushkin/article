import graph;
import unicode;
import interpolate; 
texpreamble("\usepackage{mathtext}\usepackage[russian]{babel}");
defaultpen(font("T2A","cmr","m","n"));
locale("it_IT");
usepackage("icomma");
defaultpen(0.4pt + fontsize(10pt));

real[] z = { 0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2, 0.21, 0.22, 0.23, 0.24, 0.25 };
real[] q_x = { 0.00278, 0.00311, 0.00336, 0.00349, 0.00351, 0.00341, 0.00320, 0.00289, 0.00252, 0.00211, 0.00169, 0.00130, 0.00094, 0.00063, 0.00038, 0.00018, 0.00003, -0.00008, -0.00016, -0.00021, -0.00024, -0.00026, -0.00027, -0.00028, -0.00028, -0.00028 }; 
real[] q_z = { -0.00556, -0.00622, -0.00671, -0.00699, -0.00703, -0.00682, -0.00639, -0.00578, -0.00503, -0.00422, -0.00339, -0.00260, -0.00188, -0.00127, -0.00076, -0.00036, -0.00006, 0.00016, 0.00031, 0.00042, 0.00048, 0.00052, 0.00055, 0.00056, 0.00057, 0.00057 };

picture pic1;

// графики
draw(pic1, graph(z, q_x, Hermite(monotonic)),linewidth(0.8pt));
draw(pic1, graph(z, q_z, Hermite(monotonic)),linewidth(0.8pt));

// пределы осей
xlimits(pic1, 0, 0.25);
ylimits(pic1, -0.008, 0.004);

// подписи к осям
xaxis(pic1,Label(shift(10,-2)*"$z, мм$",1),BottomTop);
yaxis(pic1,Label(shift(55,12)*"$q_x \cdot 10^3, q_z \cdot 10^3$",1),LeftRight);
label(pic1, "$0$",(0.0, -0.008), S);
label(pic1, "$0.05$",(0.05, -0.008), S);
label(pic1, "$0.1$",(0.1, -0.008), S);
label(pic1, "$0.15$",(0.15, -0.008), S);
label(pic1, "$0.2$",(0.2, -0.008), S);
label(pic1, "$-8$",(0, -0.008), W);
label(pic1, "$-6$",(0, -0.006), W);
label(pic1, "$-4$",(0, -0.004), W);
label(pic1, "$-2$",(0, -0.002), W);
label(pic1, "$0$",(0, 0), W);
label(pic1, "$2$",(0, 0.002), W);

// штриховка
xequals(pic1, 0.05, Dotted);
xequals(pic1, 0.1, Dotted);
xequals(pic1, 0.15, Dotted);
xequals(pic1, 0.2, Dotted);
yequals(pic1, -0.006, Dotted);
yequals(pic1, -0.004, Dotted);
yequals(pic1, -0.002, Dotted);
yequals(pic1, 0);
yequals(pic1,  0.002, Dotted);

draw(pic1, (0.11, 0.00130)--(0.13, 0.0022), linewidth(0.4pt));
label(pic1, "\small $1$",(0.13, 0.0022), NE);
draw(pic1, (0.11, -0.00260)--(0.13, -0.0033), linewidth(0.4pt));
label(pic1, "\small $2$",(0.13, -0.0033), SE);

// размер изображения
size(pic1,6cm,7cm, point(pic1,SW), point(pic1,NE));
frame f1=pic1.fit();
add(f1);

