import graph;
import unicode;
import interpolate; 
texpreamble("\usepackage{mathtext}\usepackage[russian]{babel}");
defaultpen(font("T2A","cmr","m","n"));
locale("it_IT");
usepackage("icomma");
defaultpen(0.4pt + fontsize(10pt));

real[] z_exp = { 0, 0.013461538, 0.036538462, 0.05, 0.063461538, 0.090384615, 0.118, 0.140078431, 0.161764706, 0.188461538, 0.2 };
real[] sigma_x_exp = { -872, -1000, -1111.54, -1069.23, -1000, -800, -600, -400, -200, 0, 34.78 }; 
real[] z_calc = { 0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2, 0.21, 0.22, 0.23, 0.24, 0.25 };
real[] sigma_x_calc = { -877.571863, -981.8391419, -1059.408027, -1103.238982, -1109.207628, -1076.744441, -1008.924319, -911.986515, -794.3887565, -665.5941673, -534.8313164, -410.0490766, -297.2198751, -200.0513172, -120.0748298, -57.01338542, -9.299846167, 25.37791548, 49.59928276, 65.85395419, 76.32017306, 82.76358651, 86.52644111, 88.57294703, 89.56093666, 89.9181875 };

picture pic1;

// графики
draw(pic1, graph(z_exp, sigma_x_exp, Hermite(monotonic)),linewidth(0.8pt));
draw(pic1, graph(z_calc, sigma_x_calc, Hermite(monotonic)),dashed+linewidth(0.8pt));

// пределы осей
xlimits(pic1, 0, 0.25);
ylimits(pic1, -1200, 150);

// подписи к осям
xaxis(pic1,Label(shift(10,-2)*"$z, мм$",1),BottomTop);
yaxis(pic1,Label(shift(20,12)*"$\sigma_x$, МПа",1),LeftRight);
label(pic1, "$0$",(0, -1200), S);
label(pic1, "$0.05$",(0.05, -1200), S);
label(pic1, "$0.1$",(0.1, -1200), S);
label(pic1, "$0.15$",(0.15, -1200), S);
label(pic1, "$0.2$",(0.2, -1200), S);
label(pic1, "$-1200$",(0, -1200), W);
label(pic1, "$-900$",(0, -900), W);
label(pic1, "$-600$",(0, -600), W);
label(pic1, "$-300$",(0, -300), W);
label(pic1, "$0$",(0, 0), W);

// штриховка
xequals(pic1, 0.05, Dotted);
xequals(pic1, 0.1, Dotted);
xequals(pic1, 0.15, Dotted);
xequals(pic1, 0.2, Dotted);
yequals(pic1, -900, Dotted);
yequals(pic1, -600, Dotted);
yequals(pic1, -300, Dotted);
yequals(pic1, 0);

// размер изображения
size(pic1,6cm,7cm, point(pic1,SW), point(pic1,NE));
frame f1=pic1.fit();
add(f1);

