import graph;
import unicode;
import interpolate; 
texpreamble("\usepackage{mathtext}\usepackage[russian]{babel}");
defaultpen(font("T2A","cmr","m","n"));
locale("it_IT");
usepackage("icomma");
defaultpen(0.4pt + fontsize(10pt));

real[] z_exp = { 0, 0.0145, 0.035, 0.05, 0.052, 0.073, 0.089, 0.1, 0.125, 0.15, 0.151, 0.17, 0.19, 0.2, 0.21 };
real[] sigma_x_exp = { -840, -889, -955.024, -996.871, -1000, -1032.258, -1000, -957.452, -800, -600, -586.9, -400, -200, -95.7, 0 }; 
real[] z_calc = { 0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2, 0.21, 0.22, 0.23, 0.24, 0.25, 0.26, 0.27, 0.28, 0.29, 0.3 };
real[] sigma_x_calc = { -840.0154531, -899.8308302, -950.2758399, -989.694382, -1016.754994, -1030.525321, -1030.525321, -1016.754994, -989.694382, -950.2758399, -899.8308302, -840.0154531, -772.7205312, -699.9730413, -623.8360246, -546.313785, -469.2682761, -394.3512196, -322.9548621, -256.1825461, -194.8386301, -139.4358881, -90.21745791, -47.18974555, -10.16243618, 21.20812808, 47.37553778, 68.86259614, 86.22528956, 100.0227041, 110.7934501 };

picture pic1;

// графики
draw(pic1, graph(z_exp, sigma_x_exp, Hermite(monotonic)),linewidth(0.8pt));
draw(pic1, graph(z_calc, sigma_x_calc, Hermite(monotonic)),dashed+linewidth(0.8pt));

// пределы осей
xlimits(pic1, 0, 0.3);
ylimits(pic1, -1200, 150);

// подписи к осям
xaxis(pic1,Label(shift(10,-2)*"$z, мм$",1),BottomTop);
yaxis(pic1,Label(shift(20,12)*"$\sigma_x$, МПа",1),LeftRight);
label(pic1, "$0$",(0, -1200), S);
label(pic1, "$0.05$",(0.05, -1200), S);
label(pic1, "$0.1$",(0.1, -1200), S);
label(pic1, "$0.15$",(0.15, -1200), S);
label(pic1, "$0.2$",(0.2, -1200), S);
label(pic1, "$0.25$",(0.25, -1200), S);
label(pic1, "$-1200$",(0, -1200), W);
label(pic1, "$-900$",(0, -900), W);
label(pic1, "$-600$",(0, -600), W);
label(pic1, "$-300$",(0, -300), W);
label(pic1, "$0$",(0, 0), W);

// штриховка
xequals(pic1, 0.05, Dotted);
xequals(pic1, 0.1, Dotted);
xequals(pic1, 0.15, Dotted);
xequals(pic1, 0.2, Dotted);
xequals(pic1, 0.25, Dotted);
yequals(pic1, -900, Dotted);
yequals(pic1, -600, Dotted);
yequals(pic1, -300, Dotted);
yequals(pic1, 0);

// размер изображения
size(pic1,6cm,7cm, point(pic1,SW), point(pic1,NE));
frame f1=pic1.fit();
add(f1);

