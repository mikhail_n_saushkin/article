import graph;
import unicode;
import interpolate; 
texpreamble("\usepackage{mathtext}\usepackage[russian]{babel}");
defaultpen(font("T2A","cmr","m","n"));
locale("it_IT");
usepackage("icomma");
defaultpen(0.4pt + fontsize(10pt));

real[] z_exp = { 0, 0.0227848, 0.04038, 0.05, 0.068, 0.097835, 0.1, 0.12305, 0.148, 0.15, 0.168987, 0.19277213, 0.2 };
real[] sigma_x_exp = { -840, -1000, -1056, -1048, -1000, -790, -778, -600, -400, -380, -200, 0, 32 }; 
real[] z_calc = { 0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2, 0.21, 0.22, 0.23, 0.24, 0.25 };
real[] sigma_x_calc = { -840.3452765, -928.500083, -996.7778718, -1040.337252, -1055.978965, -1042.533975, -1001.007774, -934.4561872, -847.6139604, -746.3388142, -636.9615315, -525.6417337, -417.8187093, -317.8209452, -228.6639382, -152.0315233, -88.40826565, -37.3137457, 2.415106105, 32.34181221, 54.18404358, 69.62383877, 80.18073239, 87.14318764, 91.54698882, 94.18659076 };

picture pic1;

// графики
draw(pic1, graph(z_exp, sigma_x_exp, Hermite(monotonic)),linewidth(0.8pt));
draw(pic1, graph(z_calc, sigma_x_calc, Hermite(monotonic)),dashed+linewidth(0.8pt));

// пределы осей
xlimits(pic1, 0, 0.25);
ylimits(pic1, -1200, 150);

// подписи к осям
xaxis(pic1,Label(shift(10,-2)*"$z, мм$",1),BottomTop);
yaxis(pic1,Label(shift(20,12)*"$\sigma_x$, МПа",1),LeftRight);
label(pic1, "$0$",(0, -1200), S);
label(pic1, "$0.05$",(0.05, -1200), S);
label(pic1, "$0.1$",(0.1, -1200), S);
label(pic1, "$0.15$",(0.15, -1200), S);
label(pic1, "$0.2$",(0.2, -1200), S);
label(pic1, "$-1200$",(0, -1200), W);
label(pic1, "$-900$",(0, -900), W);
label(pic1, "$-600$",(0, -600), W);
label(pic1, "$-300$",(0, -300), W);
label(pic1, "$0$",(0, 0), W);

// штриховка
xequals(pic1, 0.05, Dotted);
xequals(pic1, 0.1, Dotted);
xequals(pic1, 0.15, Dotted);
xequals(pic1, 0.2, Dotted);
yequals(pic1, -900, Dotted);
yequals(pic1, -600, Dotted);
yequals(pic1, -300, Dotted);
yequals(pic1, 0);

// размер изображения
size(pic1,6cm,7cm, point(pic1,SW), point(pic1,NE));
frame f1=pic1.fit();
add(f1);

