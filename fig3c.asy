import graph;
import unicode;
import interpolate; 
texpreamble("\usepackage{mathtext}\usepackage[russian]{babel}");
defaultpen(font("T2A","cmr","m","n"));
locale("it_IT");
usepackage("icomma");
defaultpen(0.4pt + fontsize(10pt));

real[] z_exp = { 0, 0.0398734, 0.05, 0.05769, 0.07215189, 0.1, 0.11392, 0.1386, 0.15, 0.15949, 0.180379, 0.19556, 0.2038 };
real[] sigma_x_exp = { -840.4032, -1000, -1026.2616, -1024.3434, -1000, -883.8072, -800, -600, -493.848, -400, -200, -40.26, 0 }; 
real[] z_calc = { 0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2, 0.21, 0.22, 0.23, 0.24, 0.25, 0.26, 0.27, 0.28, 0.29, 0.3 };
real[] sigma_x_calc = { -825.044175, -884.1013617, -934.5560719, -974.8453915, -1003.684314, -1020.133594, -1023.650414, -1014.118063, -991.8523517, -957.5842782, -912.4202724, -857.7830315, -795.3373615, -726.9064075, -654.3841362, -579.649879, -504.4902094, -430.5324816, -359.1931294, -291.6424468, -228.7861967, -171.2631363, -119.4565273, -73.51695857, -33.39339938, 1.130706026, 30.39932102, 54.84631417, 74.96068653, 91.25605056, 104.2452863 };

picture pic1;

// графики
draw(pic1, graph(z_exp, sigma_x_exp, Hermite(monotonic)),linewidth(0.8pt));
draw(pic1, graph(z_calc, sigma_x_calc, Hermite(monotonic)),dashed+linewidth(0.8pt));

// пределы осей
xlimits(pic1, 0, 0.3);
ylimits(pic1, -1200, 150);

// подписи к осям
xaxis(pic1,Label(shift(10,-2)*"$z, мм$",1),BottomTop);
yaxis(pic1,Label(shift(20,12)*"$\sigma_x$, МПа",1),LeftRight);
label(pic1, "$0$",(0, -1200), S);
label(pic1, "$0.05$",(0.05, -1200), S);
label(pic1, "$0.1$",(0.1, -1200), S);
label(pic1, "$0.15$",(0.15, -1200), S);
label(pic1, "$0.2$",(0.2, -1200), S);
label(pic1, "$0.25$",(0.25, -1200), S);
label(pic1, "$-1200$",(0, -1200), W);
label(pic1, "$-900$",(0, -900), W);
label(pic1, "$-600$",(0, -600), W);
label(pic1, "$-300$",(0, -300), W);
label(pic1, "$0$",(0, 0), W);

// штриховка
xequals(pic1, 0.05, Dotted);
xequals(pic1, 0.1, Dotted);
xequals(pic1, 0.15, Dotted);
xequals(pic1, 0.2, Dotted);
xequals(pic1, 0.25, Dotted);
yequals(pic1, -900, Dotted);
yequals(pic1, -600, Dotted);
yequals(pic1, -300, Dotted);
yequals(pic1, 0);

// размер изображения
size(pic1,6cm,7cm, point(pic1,SW), point(pic1,NE));
frame f1=pic1.fit();
add(f1);

