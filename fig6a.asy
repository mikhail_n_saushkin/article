import graph;
import unicode;
import interpolate; 
texpreamble("\usepackage{mathtext}\usepackage[russian]{babel}");
defaultpen(font("T2A","cmr","m","n"));
locale("it_IT");
usepackage("icomma");
defaultpen(0.4pt + fontsize(10pt));

real[] z_exp = { 0, 0.003, 0.006, 0.012, 0.021, 0.029, 0.047, 0.05, 0.062, 0.076, 0.088, 0.1, 0.103, 0.115, 0.126, 0.147, 0.15, 0.165, 0.176, 0.188, 0.2, 0.212, 0.217, 0.22, 0.223, 0.226, 0.229 };
real[] sigma_x_exp = { -517, -533.45, -549.9, -580.45, -622.75, -650.95, -674.45, -674.45, -662.7, -632.15, -599.25, -561.65, -552.25, -505.25, -470, -387.75, -376, -303.15, -258.5, -188, -117.5, -39.95, 0, 8, 8, 8, 8 }; 

real[] z_calc = { 0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2, 0.21, 0.22, 0.23, 0.24, 0.25 };
real[] sigma_x_calc = { -684.1585233, -687.3463809, -688.0443624, -688.2332049, -688.2814701, -688.2599862, -688.1387346, -687.7076832, -685.8950895, -676.4820021, -633.1760514, -539.4032424, -432.3557376, -331.1245818, -240.3319527, -161.9045343, -96.4827234, -43.7038443, -2.4825186, 28.70406, 51.5531196, 67.7447208, 78.8306094, 86.152401, 90.7972398, 93.5983872 };

picture pic1;

// графики
draw(pic1, graph(z_exp, sigma_x_exp, Hermite(monotonic)),linewidth(0.8pt));
draw(pic1, graph(z_calc, sigma_x_calc, Hermite(monotonic)),dashed+linewidth(0.8pt));

// пределы осей
xlimits(pic1, 0, 0.25);
ylimits(pic1, -800, 200);

// подписи к осям
xaxis(pic1,Label(shift(10,-2)*"$z, мм$",1),BottomTop);
yaxis(pic1,Label(shift(20,12)*"$\sigma_x$, МПа",1),LeftRight);
label(pic1, "$0$",(0, -800), S);
label(pic1, "$0.05$",(0.05, -800), S);
label(pic1, "$0.1$",(0.1, -800), S);
label(pic1, "$0.15$",(0.15, -800), S);
label(pic1, "$0.2$",(0.2, -800), S);
label(pic1, "$-800$",(0, -800), W);
label(pic1, "$-600$",(0, -600), W);
label(pic1, "$-400$",(0, -400), W);
label(pic1, "$-200$",(0, -200), W);
label(pic1, "$0$",(0, 0), W);

// штриховка
xequals(pic1, 0.05, Dotted);
xequals(pic1, 0.1, Dotted);
xequals(pic1, 0.15, Dotted);
xequals(pic1, 0.2, Dotted);
yequals(pic1, -600, Dotted);
yequals(pic1, -400, Dotted);
yequals(pic1, -200, Dotted);
yequals(pic1, 0);

// размер изображения
size(pic1,6cm,7cm, point(pic1,SW), point(pic1,NE));
frame f1=pic1.fit();
add(f1);

